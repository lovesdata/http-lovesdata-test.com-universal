<html>
<head>
<title>Simple PHP Shopping Cart - Demo</title>
</head>
<?php

include("simplecart_files/header.php");

?>

<style type="text/css">
p{
font-size:13px;
font-family:arial;
text-align:center;
}

.simpleshop_product{
	border:1px dashed #dddddd;
	padding:5px;
	text-align:center;
	font-size:13px;
	font-family:arial;
}
.simpleshop_product_name{
	font-weight:bold;
}

</style>

<p>These are example products, click add to cart to test the checkout process.</p>
<p>The HTML and CSS for these products can be easily changed by you. Try the Admin Page and see how easy it is!</p>
<p><a href="cart.php?simpleshop_admin">Click here for Administration Page</a> | <a href="http://codecanyon.net/user/dtbaker?ref=dtbaker">Click here to Purchase this Software</a></p>

<table cellpadding="10" width="100%">
	<tr>
		<td width="33%" valign="top">
			<form method="post" action="cart.php"> <input type="hidden" value="add" name="simpleshop_action"> <input type="hidden" value="" name="url"> <input type="hidden" value="YTozOntzOjQ6Im5hbWUiO3M6MTI6IkJsdWUgVC1TaGlydCI7czo4OiJzaGlwcGluZyI7czoxOiI0IjtzOjQ6ImF0dHIiO2E6Mzp7aTowO2E6Mjp7czo0OiJuYW1lIjtzOjExOiJTaXplIE1lZGl1bSI7czo1OiJwcmljZSI7czo1OiIxNi41MyI7fWk6MTthOjI6e3M6NDoibmFtZSI7czoxMDoiU2l6ZSBMYXJnZSI7czo1OiJwcmljZSI7czo1OiIxNy45NSI7fWk6MjthOjI6e3M6NDoibmFtZSI7czoxMjoiU2l6ZSBYLUxhcmdlIjtzOjU6InByaWNlIjtzOjU6IjE4Ljk1Ijt9fX0=|19a603e3894ed82b668b480f63d14d3b" name="simpleshop_product_settings"> <div class="simpleshop_product"> <div class="simpleshop_product_name">Blue T-Shirt</div> <div class="simpleshop_product_price"> <select name="product_option"><option value="0">Size Medium - $16.53</option><option value="1">Size Large - $17.95</option><option value="2">Size X-Large - $18.95</option></select></div> <div class="simpleshop_product_quantity"> Quantity: <input type="text" size="3" value="1" name="quantity"> </div> <div class="simpleshop_product_add"> <input type="submit" value="Add To Cart" name="add_product"> </div> </div> </form>
		</td>
		<td width="33%" valign="top">
			<form method="post" action="cart.php"> <input type="hidden" value="add" name="simpleshop_action"> <input type="hidden" value="" name="url"> <input type="hidden" value="YTozOntzOjQ6Im5hbWUiO3M6MTI6IkF3ZXNvbWUgQm9vayI7czo4OiJzaGlwcGluZyI7czoxOiIzIjtzOjQ6ImF0dHIiO2E6MTp7aTowO2E6Mjp7czo0OiJuYW1lIjtzOjA6IiI7czo1OiJwcmljZSI7czo1OiIxOC45NSI7fX19|ea03fc9ec0b123f551d65663b92bb54f" name="simpleshop_product_settings"> <div class="simpleshop_product"> <div class="simpleshop_product_name">Awesome Book</div> <div class="simpleshop_product_price"> $18.95</div> <div class="simpleshop_product_quantity"> Quantity: <input type="text" size="3" value="1" name="quantity"> </div> <div class="simpleshop_product_add"> <input type="submit" value="Add To Cart" name="add_product"> </div> </div> </form>
		</td>
		<td width="33%" valign="top">
			<form method="post" action="cart.php"> <input type="hidden" value="add" name="simpleshop_action"> <input type="hidden" value="" name="url"> <input type="hidden" value="YTozOntzOjQ6Im5hbWUiO3M6MTY6Ikdvcmdlb3VzIEZsb3dlcnMiO3M6ODoic2hpcHBpbmciO3M6MzoiMi41IjtzOjQ6ImF0dHIiO2E6Mzp7aTowO2E6Mjp7czo0OiJuYW1lIjtzOjc6IjYgUm9zZXMiO3M6NToicHJpY2UiO3M6MjoiMTIiO31pOjE7YToyOntzOjQ6Im5hbWUiO3M6ODoiMTIgUm9zZXMiO3M6NToicHJpY2UiO3M6MjoiMjAiO31pOjI7YToyOntzOjQ6Im5hbWUiO3M6ODoiMjQgUm9zZXMiO3M6NToicHJpY2UiO3M6NToiMzUuOTUiO319fQ==|0fd78c1381e94231b5ff7d2099f756b3" name="simpleshop_product_settings"> <div class="simpleshop_product"> <div class="simpleshop_product_name">Gorgeous Flowers</div> <div class="simpleshop_product_price"> <select name="product_option"><option value="0">6 Roses - $12</option><option value="1">12 Roses - $20</option><option value="2">24 Roses - $35.95</option></select></div> <div class="simpleshop_product_quantity"> Quantity: <input type="text" size="3" value="1" name="quantity"> </div> <div class="simpleshop_product_add"> <input type="submit" value="Add To Cart" name="add_product"> </div> </div> </form>
		</td>
	</tr>
</table>

<?php

include("simplecart_files/footer.php");

?>


</html>