PHP Simple Shop.
Author: dtbaker for CodeCanyon.net
Thank you for purchasing my item!


HOW TO INSTALL:
-----------------------------

# Unzip the file you download from CodeCanyon
# Upload cart.php to your website (eg: http://yourwebsite.com/cart.php)
# Upload the simplecart_files/ folder to your website (eg: http://yourwebsite.com/simplecart_files)
# Visit your admin page and setup the shop: http://yourwebsite.com/cart.php?simpleshop_admin
  (replace yourwebsite.com with your actual website above)
  
  
PLEASE VIEW THE FULL README FILE BY GOING TO THIS LINK:
http://tf.dtbaker.com.au/widget_simplecart/README.html


HELP AND SUPPORT:
-----------------------------
You can leave a comment on the item and I can reply, or for more urgent matters you can contact me through my CodeCanyon profile here:
http://codecanyon.net/user/dtbaker
Item support by Authors is totally optional through CodeCanyon - I do provide support (if you are nice) but it may take me a day or two to reply to questions.


ICONS/FONTS USED:
-----------------------------
http://famfamfam.com/lab/icons/silk/ (Creative Commons Attribution 3.0 License)
http://www.dafont.com/androgyne-tb.font (used in Admin page - donation-ware - donation has been made)

