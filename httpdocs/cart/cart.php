<?php


/********** Simple Shopping Cart Widget *************
 * Author: dtbaker
 * Date: 9 Feb 2010
 * Version: 1.11
 * Website: http://codecanyon.net
 * Handcoded with love by dave :) Yes I know - one big ugly file. The advanced cart (in development) splits this up into a nice class.
 * All copyright etc.. dtbaker.com.au
 * You have purchased a SINGLE USE license. To use this file on multiple websites you have to purchase an additional license. This is a condition of receiving free updates when they are released on CodeCanyon. 
 * This file is to only be distributed by dtbaker through CodeCanyon
 * It cannot be re-distributed part or whole in any other software package.
 * This comment block must not be removed. 
 * Instructions located in README file.
 * For support, please post a comment on the CodeCanyon item board. 
 *********************************************/


// include the configuration file, if there is no configuration file found then we redirect straight to the admin page
define('_CONFIG_FOLDER','simplecart_files/');
$config_file = _CONFIG_FOLDER.'cart.cfg.php';
if(!is_file($config_file)){
	// redirect to admin page
	if(!isset($_REQUEST['simpleshop_admin'])){
		header("Location: ?simpleshop_admin");
		exit;
	}
}else{
	require_once($config_file);
}
define("_DEMO_MODE",false);
if(!defined('_SIMPLESHOP_URL')){
	// absolute path to this file on your website.
	// should find a reliable way for win/linux to work this out automatically. meh. 
	$current_directory = dirname($_SERVER['REQUEST_URI']);
	define("_SIMPLESHOP_URL",$current_directory.'/cart.php'); 
}
if(!defined('_SIMPLESHOP_BASE')){
	define('_SIMPLESHOP_BASE','http'.( ($_SERVER["HTTPS"] || $_SERVER['SERVER_PORT']==443) ? 's' : '').'://'.$_SERVER['HTTP_HOST']);
}


/****** BEGIN CODE - try not to change too much below here *********/

// set the error mode so I behave when coding:
$simpleshop_ini_error = ini_set("error_reporting",E_ALL);
$simpleshop_ini_display = ini_set("display_errors",true);

if(!session_id()){
	// work out if we can start a session
	$simpleshop_res = headers_sent($simpleshop_file,$simpleshop_line);
	if($simpleshop_res){
		// headers have been sent, display a message prompting the user to add session_start();
		echo 'Please add <?php session_start(); ?> before line "'.$simpleshop_line.'" in file "'.$simpleshop_file.'" to use the shop correctly.';
		exit;
	}else{
		session_start();
	}
}

$cart_products = isset($_SESSION['_simpleshop_cart_products']) ? $_SESSION['_simpleshop_cart_products'] : array();
if(!is_array($cart_products)){
	$cart_products=array();
}

if(isset($_REQUEST['simpleshop_action']) && $_REQUEST['simpleshop_action']){
	$refresh = false;
	// what are we doing?
	switch($_REQUEST['simpleshop_action']){
		case "add":
			if(isset($_REQUEST['simpleshop_product_settings'])){
				$product_settings = explode("|",$_REQUEST['simpleshop_product_settings']);
				// hash to see if it's true. stops people modifying prices.
				if($product_settings[1] == md5('my not so s3cret code heh'.$product_settings[0])){
					$product_settings = unserialize(base64_decode($product_settings[0]));
				}else{
					$product_settings = false;
				}
				if($product_settings){
					// which option have they selected? or use the default option 1 if none are selected
					$product_option = (isset($_REQUEST['product_option'])) ? (int)$_REQUEST['product_option'] : 0;
					if(!isset($product_settings['attr'])){
						$product_settings['attr'] = array();
					}
					if(!isset($product_settings['attr'][$product_option]) && isset($product_settings['attr'][0])){
						$product_option = 0;
					}
					$option_settings = $product_settings['attr'][$product_option];
					if($option_settings && is_array($option_settings)){
						// now we work out what values to add to the shopping cart session array.
						// we want the name of the product
						$product_name = $product_settings['name'];
						// we want it's price, the price of the option that the user selected:
						$price = $option_settings['price'];
						// we see if there is a name of the option that the user selected.
						if(isset($option_settings['name']) && $option_settings['name'] && $option_settings['name'] != $product_name){
							$product_name .= ' - ' . $option_settings['name'];
						}
						// and work out the quantity ordered, with a defualt of 1.
						$quantity = max(1, ((int)$_REQUEST['quantity']) ? (int)$_REQUEST['quantity'] : 1 ); 
						$product_uri = (isset($_REQUEST['url']))?$_REQUEST['url']:'';
						$new_product = array(
							// values that are showed to the user in checkout:
							"name"=>$product_name,
							"price"=>$price,
							"quantity"=>$quantity,
							"product_uri"=>$product_uri,
							"product_option"=>$product_option,
							"product_settings"=>$product_settings,
						);
						// for now we just overwrite this product with a new one.
						$cart_products [md5(serialize(array($product_option,$product_settings)))] = $new_product;
						// refresh the page so it doesn't add the product twice on refresh.
						$refresh = true;
					}
				}
			}
			break;
		case "modify_quantities":
			if(isset($_REQUEST['product_quantities'])){
				$modify_product_id = $_REQUEST['product_quantities'];
				if(is_array($modify_product_id)){
					foreach($modify_product_id as $key => $val){
						// key is the product id, val is the new quantity
						if(!isset($cart_products[$key]))continue;
						// quantities can only be integers:
						$val = (int)$val;
						// find the product we wish to modify
						$modify_product = $cart_products[$key];
						// check if it exists:
						if(is_array($modify_product)){
							// we are updating the quantities
							if($val == 0){
								// uset set the quantity to 0, remove from cart
								unset($cart_products[$key]);
							}else{
								// update the quantity with the new value:
								$modify_product['quantity'] = $val;
								// load it back into the products array:
								$cart_products[$key] = $modify_product;
							}
						}
					}
				}
				$refresh=true;
			}
			break;
	}
	// the above actions may change the state of the shopping cart, so we load it back into the session for next time:
	$_SESSION['_simpleshop_cart_products'] = $cart_products;
	// reload the page with a flag to stop any possible redirect loops.
	if($refresh && !isset($_REQUEST['simpleshop_redir'])){
		if(isset($_REQUEST['simpleshop_checkout_go'])){
			header("Location: "._SIMPLESHOP_BASE._SIMPLESHOP_URL . ( (strpos(_SIMPLESHOP_URL,'?')===false) ? '?' : '&' ) . "simpleshop_checkout");
		}else{
			header("Location: "._SIMPLESHOP_BASE._SIMPLESHOP_URL . ( (strpos(_SIMPLESHOP_URL,'?')===false) ? '?' : '&' ) . "simpleshop_redir=true");
		}
		exit;
	}
}
$cart_sub_total = 0;
$cart_total = 0;
$cart_shipping = 0;
foreach($cart_products as $product_id => $product){ 
	$quantity = max(1,(int)$product['quantity']);
	$product_total = $product['price'] * $quantity;
	$cart_sub_total += $product_total;
	if(isset($product['product_settings']['shipping']) && $product['product_settings']['shipping']>0){
		$cart_shipping += $product['product_settings']['shipping'];
	}
} 
$cart_total = $cart_sub_total + $cart_shipping;
$member = isset($_SESSION['_cart_member_data']) ? $_SESSION['_cart_member_data'] : array();
// quick header / footer hack:
function _simpleshop_header($admin=false){
	if($admin){
		?>
		<html>
		<head>
			<title>Simpleshop - Admin</title>
			<link rel="stylesheet" href="<?php echo _CONFIG_FOLDER;?>styles.css">
			<!--[if IE]>
			<style type="text/css"> 
			body#simpleshop_full #menu_container li:hover,
			body#simpleshop_full #menu_container li.current,
			body#simpleshop_full #menu_container li a:hover,
			body#simpleshop_full #menu_container li.current a,
			body#simpleshop_full #menu_container li a,
			body#simpleshop_full #menu_container li{
				background-image:none;
			}
			body#simpleshop_full #menu_container li a:hover{
				color:#FFFFFF;
			}
			</style>
			<![endif]-->

			<script src="http://www.google.com/jsapi"></script>
			<script>
			  google.load("jquery", "1");
			  function set_add_del(id){
				$("#"+id+' .remove_addit').show();
				$("#"+id+' .add_addit').hide();
				$("#"+id+' .add_addit:last').show();
				$("#"+id+" .dynamic_block:only-child > .remove_addit").hide();
			}
			function selrem(clickety,id){
				$(clickety).parents('.dynamic_block').remove(); 
				set_add_del(id); 
				return false;
			}
			function seladd(clickety,id){
				var box = $('#'+id+' .dynamic_block:last').clone(true);
				$('input',box).val('');
				$('#'+id+' .dynamic_block:last').after(
			       box); 
				set_add_del(id); 
				return false;
			}
			function reset_names(){
				var id = 0;
				$('.dynamic_block').each(function(){
					$('input',this).each(function(){
						$(this).attr('name','attr['+id+']['+$(this).attr('rel')+']');
					});
					id++;
				});
			}
			</script>
		</head>
		<body id="simpleshop_full">
		
		<div id="wrapper">
		<div id="header">
		<div id="logo"><img src="<?php echo _CONFIG_FOLDER;?>logo_simple_shop.gif" width="229" height="46" alt="Simple Shop" /></div>
		<div id="admin_page"><img src="<?php echo _CONFIG_FOLDER;?>admin_page.gif" width="221" height="45" alt="Admin Page" /></div>
		</div><!--end header-->
		
		<?php
	}else{
		// include a header file if one exists.
		if(is_file(_CONFIG_FOLDER.'header.php')){
			include(_CONFIG_FOLDER.'header.php');
		}
	}
}
function _simpleshop_footer($admin=false){
	if($admin){
		?>
		<hr class="clear">
		</div>
		</body>
		</html>
		<?php
	}else{
		// include a header file if one exists.
		if(is_file(_CONFIG_FOLDER.'footer.php')){
			include(_CONFIG_FOLDER.'footer.php');
		}
	}
}

ob_start(); // make header redirects etc easier later on.

// some actions we have here, pass the functionality off to include files.
// this lets people customise the include files if they need, and upgrade this
// class as I release future versions.
if(isset($_REQUEST['simpleshop_admin'])){
	$a = (isset($_REQUEST['a'])) ? $_REQUEST['a'] : false;
	// load up the admin page, prompt for their password.
	if($a == 'logout'){
		$_SESSION['_simpleshop_admin_loggedin'] = false;
		header("Location: ?simpleshop_admin"); 
		exit;
	}
	$loggedin = (isset($_SESSION['_simpleshop_admin_loggedin'])) ? $_SESSION['_simpleshop_admin_loggedin'] : false;
	if(!defined('_SIMPLESHOP_PASSWORD')){
		$loggedin = true;
		if($a != 'setup'){
			header("Location: ?simpleshop_admin&a=setup");
			exit;
		}
	}
	if(!$loggedin && isset($_REQUEST['password']) && _SIMPLESHOP_PASSWORD==$_REQUEST['password']){
		$_SESSION['_simpleshop_admin_loggedin'] = true;
		header("Location: ?simpleshop_admin"); // prevent postback refresh errors. incorrect use of ? i know, but it works.
		exit;
	}
	_simpleshop_header(true);

	if(!$loggedin){
		?>
		<div id="container">
			<div class="top"><img src="<?php echo _CONFIG_FOLDER;?>content_top.jpg" width="960" height="5" alt="_" /></div>
			<div class="content">
			<div style="text-align:center;">
			<form action="?simpleshop_admin" method="post">
				Password: <input type="password" name="password" class="shop_input" value="<?php echo (_DEMO_MODE)?_SIMPLESHOP_PASSWORD:'';?>"> <input type="submit" name="login" value="Login">
			</form>
			</div>
			<hr class="clear">
		</div>
		<?php 
	}else{
		?>
		<div id="menu_container">
			<ul>
				<li <?php echo (!$a)?'class="current"':'';?>><a href="?simpleshop_admin"><span><img src="<?php echo _CONFIG_FOLDER;?>icon_cart.png" border="0"></span>Create a New Product</a></li>
				<li <?php echo ($a=='setup')?'class="current"':'';?>><a href="?simpleshop_admin&a=setup"><span><img src="<?php echo _CONFIG_FOLDER;?>icon_settings.png" border="0"></span>Settings</a></li>
				<li <?php echo ($a=='help')?'class="current"':'';?>><a href="?simpleshop_admin&a=help"><span><img src="<?php echo _CONFIG_FOLDER;?>icon_help.png" border="0"></span>Help</a></li>
				<li><a href="<?php echo _PAYMENT_CONTINUE_URL;?>" target="_blank"><span><img src="<?php echo _CONFIG_FOLDER;?>icon_cart.png" border="0"></span>View Shop</a></li>
				<li><a href="?simpleshop_admin&a=logout"><span><img src="<?php echo _CONFIG_FOLDER;?>icon_logout.png" border="0"></span>Logout</a></li>
			</ul>
		</div><!--end menu-->
		<div id="container">
			<div class="top"><img src="<?php echo _CONFIG_FOLDER;?>content_top.jpg" width="960" height="5" alt="_" /></div>
			<div class="content">
		
		
	
			<?php 
			
			switch($a){
				case 'setup':
					if(isset($_REQUEST['download'])){
						ob_end_clean();
						header("Content-type: text/plain");
						header('Content-Disposition: attachment; filename="'.basename($config_file).'"');
						echo base64_decode($_REQUEST['download']);
						exit;
					}
					if(isset($_REQUEST['save'])){
						$config_content = '<?php '."\n\n";
						foreach($_REQUEST['config'] as $key => $val){
							$config_content .= 'define("'.$key.'","'.$val.'");'."\n";
						}
						if(!_DEMO_MODE && is_writable($config_file)){
							file_put_contents($config_file,$config_content);
							echo "Configuration has been successfully saved, please <a href='?simpleshop_admin'>click here</a> to continue.";
						}else{
							echo "Please <a href='?simpleshop_admin&a=setup&download=".base64_encode($config_content)."'>click here</a> to download '".basename($config_file)."' and upload it to the '"._CONFIG_FOLDER."' folder. Please <a href='?simpleshop_admin'>click here</a> after you have uploaded this file.";
							echo "<br><br>Note: if you set permissions on this file correctly, it will save automatically so you don't have to upload it each time.";
						}
					}else{ 
						?>
						<h2>Your Shop Settings:</h2>
						<form action="" method="post">
						<input type="hidden" name="save" value="save">
						<table cellpadding="5">
							<tr>
								<td valign="top" width="50%">
									<h3>Shop Options:</h3>
									<table>
										<tr>
											<td>Your Email:</td>
											<td><input type="text" class="shop_input" name="config[_SHOP_EMAIL]" value="<?php echo (defined('_SHOP_EMAIL'))?_SHOP_EMAIL:'';?>"></td>
										</tr>
										<tr>
											<td>Shop Name:</td>
											<td><input type="text" class="shop_input" name="config[_SHOP_NAME]" value="<?php echo (defined('_SHOP_NAME'))?_SHOP_NAME:'';?>"></td>
										</tr>
										<tr>
											<td>Order Email Subject:</td>
											<td><input type="text" class="shop_input" name="config[_SHOP_SUBJECT]" value="<?php echo (defined('_SHOP_SUBJECT'))?_SHOP_SUBJECT:'Your Website Order';?>"></td>
										</tr>
										<tr>
											<td>Currency:</td>
											<td><input type="text" class="shop_input" name="config[_PAYMENT_CURRENCY]" value="<?php echo (defined('_PAYMENT_CURRENCY'))?_PAYMENT_CURRENCY:'USD';?>" size="6"></td>
										</tr>
										<tr>
											<td>Symbol:</td>
											<td><input type="text" class="shop_input" name="config[_PAYMENT_SYMBOL]" value="<?php echo (defined('_PAYMENT_SYMBOL'))?_PAYMENT_SYMBOL:'$';?>" size="6"></td>
										</tr>
										<tr>
											<td>Postage Name:</td>
											<td><input type="text" class="shop_input" name="config[_SIMPLESHOP_SHIPPING_NAME]" value="<?php echo (defined('_SIMPLESHOP_SHIPPING_NAME'))?_SIMPLESHOP_SHIPPING_NAME:'Standard Shipping';?>"></td>
										</tr>
										<tr>
											<td>Postage Countries:<br>(list one per line)</td>
											<td>
											<textarea name="config[_SIMPLESHOP_COUNTRIES]" class="shop_input" rows="6" cols="20"><?php echo (defined('_SIMPLESHOP_COUNTRIES'))?_SIMPLESHOP_COUNTRIES:"Australia\nUSA\nJapan\nSingapore";?></textarea></td>
										</tr>
										<tr>
											<td>Continue Shopping Link</td>
											<td>
												<input type="text" class="shop_input" name="config[_PAYMENT_CONTINUE_URL]" value="<?php echo (defined('_PAYMENT_CONTINUE_URL')) ? _PAYMENT_CONTINUE_URL : '/';?>"><br>
												URL when user clicks 'Continute Shopping'
											</td>
										</tr>
									</table>
								</td>
								<td valign="top" width="50%">
									<h3>Payment Options:</h3> 
									<p>These options will be presented to the user when they checkout.</p>
									<table>
										<tr>
											<td>PayPal Email:</td>
											<td><input type="text" class="shop_input" name="config[_PAYMENT_PAYPAL_EMAIL]" value="<?php echo (defined('_PAYMENT_PAYPAL_EMAIL'))?_PAYMENT_PAYPAL_EMAIL:'';?>"></td>
										</tr>
										<tr>
											<td colspan="2">
												Signup for PayPal by <a href="http://www.paypal.com" target="_blank">clicking here</a>
											</td>
										</tr>
										<tr>
											<td>Google Merc ID:</td>
											<td><input type="text" class="shop_input" name="config[_PAYMENT_GOOGLE_MERCHID]" value="<?php echo (defined('_PAYMENT_GOOGLE_MERCHID'))?_PAYMENT_GOOGLE_MERCHID:'';?>"></td>
										</tr>
										<tr>
											<td colspan="2">
												Follow Google instructions by <a href="http://code.google.com/apis/checkout/developer/Google_Checkout_Custom_Cart_How_To_HTML.html#signup" target="_blank">clicking here</a>
											</td>
										</tr>
										<tr>
											<td>Bank Details</td>
											<td>
											<textarea name="config[_PAYMENT_BANK_TRANSFER]" class="shop_input" rows="6" cols="20"><?php echo (defined('_PAYMENT_BANK_TRANSFER'))?_PAYMENT_BANK_TRANSFER:'';?></textarea>
											<br>These will be displayed and emailed to the user after selecting this payment method.
											</td>
										</tr>
									</table>
								</td>
							</tr>
							<tr>
								<td valign="top">
									<h3>Confirmation Email</h3>
									<p>This email is sent to the customer when they place an order. Possible fields are: FIRST_NAME CART_CONTENTS BANK_DETAILS SHOP_NAME</p>
									<textarea cols="52" rows="10" class="shop_input" name="config[_SHOP_CUSTOMER_EMAIL]"><?php echo (defined('_SHOP_CUSTOMER_EMAIL') && trim(_SHOP_CUSTOMER_EMAIL)) ? _SHOP_CUSTOMER_EMAIL : "&lt;h1&gt;SHOP_NAME&lt;/h1&gt;
Hi FIRST_NAME, 
Thank you for placing your order with SHOP_NAME.
CART_CONTENTS
BANK_DETAILS
We will confirm your order once payment is received.
Please contact us if you have any questions regarding your order.
Thank you"; ?></textarea>
									
								</td>
								<td valign="top">
									<h3>Checkout Link:</h3>
									<a href="<?php echo _SIMPLESHOP_BASE;?><?php echo _SIMPLESHOP_URL;?>" target="_blank">Click here to Checkout</a> <br>
									Add this to your website as the checkout link.
									<br><br>
									<h3>Login Details:</h3>
									<p>Please pick a password to access this page.</p>
									<input type="password" class="shop_input" name="config[_SIMPLESHOP_PASSWORD]" value="<?php echo (defined('_SIMPLESHOP_PASSWORD')) ? _SIMPLESHOP_PASSWORD : '';?>">
								</td>
							</tr>
							<tr>
								<td colspan="3" align="center">
									<input type="image" name="save" value="Save Configuration File" src="<?php echo _CONFIG_FOLDER;?>button_save.gif">
								</td>
							</tr>
						</table>
						</form>
						<?php
					}
					// end setup.
					break;
				case 'help':
					?>
					
					<h1>Simple Shop Help</h1>
					
					<a href="http://tf.dtbaker.com.au/widget_simplecart/README.html" target="_blank">View the full README by clicking here</a>
					
					<h3>How to create a new product</h3>
					<ol>
						<li>Fill in the product details</li>
						<li>Click "Generate HTML Code" button</li>
						<li>Copy the HTML code, paste this onto your website (in "Code View" if you are using Dreamweaver)</li>
						<li>Make sure your PayPal, Google, or Bank Details are correct in Settings</li>
						<li>Done</li>
					</ol>
					
					<h3>How to style the checkout pages</h3>
					<ol>
						<li>Open the <strong>simplecart_files/header.php</strong> file in Notepad or your favorite HTML editor (eg: Dreamweaver)</li>
						<li>Delete any default HTML content from this file.</li>
						<li>Copy &amp; Paste any HTML code you would like to display <strong>above</strong> the cart into this file.</li>
						<li>Open the <strong>simplecart_files/footer.php</strong> file in Notepad or your favorite HTML editor (eg: Dreamweaver)</li>
						<li>Delete any default HTML content from this file.</li>
						<li>Copy &amp; Paste any HTML code you would like to display <strong>below</strong> the cart into this file.</li>
						
						<li>Advanced users: you can do PHP includes eg: <?php echo '&lt;?php include("your_header_file.php");?>'; ?> into these files. </li>
						<li>Advanced users: you can also <?php echo '&lt;?php include("cart.php");?>'; ?> from one of your existing files.</li>
						<li>Done.</li>
					</ol>
					
					<h3>How to style the product layout</h3>
					<ol>
						<li>Create your product following the steps above</li>
						<li>Adjust the HTML to suit your needs (eg: adding a product image, changing the wording)</li>
						<li>Style the elements using CSS, some example CSS is below, but you can make it whatever you like:</li>
					</ol>
					<table cellpadding="5">
						<tr>
							<td valign="top">
								<pre>.simpleshop_product{
	border:2px dashed #CCC;
	width:150px;
	text-align:center;
	padding:10px;
}
.simpleshop_product_name{
	font-size:15px;
	font-style:italic;
	text-decoration:underline;
	color:#FF0000;
}
.simpleshop_product_add input{
	background:#FF0000;
	color:#FFFFFF;
	border:0;
	padding:4px;
}</pre>
<style type="text/css">
					pre{
					font-size:10px;
					}
#simpleshop_demo1 .simpleshop_product{
	border:2px dashed #CCC;
	width:150px;
	text-align:center;
	padding:10px;
}
#simpleshop_demo1 .simpleshop_product_name{
	font-size:15px;
	font-style:italic;
	text-decoration:underline;
	color:#FF0000;
}
#simpleshop_demo1 .simpleshop_product_add input{
	background:#FF0000;
	color:#FFFFFF;
	border:0;
	padding:4px;
}
</style>
							</td>
							<td>
								<div id="simpleshop_demo1">
								<div class="simpleshop_product">
								<div class="simpleshop_product_name">Large T-Shirt</div> <div class="simpleshop_product_price"> <select name="product_option"><option value="0">Blue - $12.95</option><option value="1">Green - $12.95</option><option value="2">Red - $12.95</option></select></div> <div class="simpleshop_product_quantity"> Quantity: <input type="text" size="3" value="1" name="quantity"> </div> <div class="simpleshop_product_add"> <input type="button" value="Add To Cart" name="add_product"> </div>
								</div>
								</div>
							</td>
						</tr>
						<tr><td colspan="2"><hr></td></tr>
						<tr>
							<td valign="top">
								<pre>.simpleshop_product{
	background:#000000;
	width:150px;
	text-align:center;
	padding:10px;
}
.simpleshop_product_name{
	font-size:14px;
	font-weight:bold;
	font-family:comic sans, arial;
	color:#FFFFFF;
	padding:4px;
	border:1px solid #444;
	background-color:#333333;
	margin:4px;
}
.simpleshop_product_quantity{
	color:#FFFFFF;
	border:0;
	padding:4px;
}
.simpleshop_product_quantity input{
	color:green;
	border:0;
	text-align:center;
	width:30px;
	font-size:12px;
}
.simpleshop_product_price select{
	color:green;
	padding:4px;
}
.simpleshop_product_add input{
	color:green;
	padding:4px;
}</pre>
<style type="text/css">
#simpleshop_demo2 .simpleshop_product{
	background:#000000;
	width:150px;
	text-align:center;
	padding:10px;
}
#simpleshop_demo2 .simpleshop_product_name{
	font-size:14px;
	font-weight:bold;
	font-family:comic sans, arial;
	color:#FFFFFF;
	padding:4px;
	border:1px solid #444;
	background-color:#333333;
	margin:4px;
}
#simpleshop_demo2 .simpleshop_product_quantity{
	color:#FFFFFF;
	border:0;
	padding:4px;
}
#simpleshop_demo2 .simpleshop_product_quantity input{
	color:green;
	border:0;
	text-align:center;
	width:30px;
	font-size:12px;
}
#simpleshop_demo2 .simpleshop_product_price select{
	color:green;
	padding:4px;
}
#simpleshop_demo2 .simpleshop_product_add input{
	color:green;
	padding:4px;
}
</style>
							</td>
							<td>
								<div id="simpleshop_demo2">
								<div class="simpleshop_product">
								<div class="simpleshop_product_name">Large T-Shirt</div> <div class="simpleshop_product_price"> <select name="product_option"><option value="0">Blue - $12.95</option><option value="1">Green - $12.95</option><option value="2">Red - $12.95</option></select></div> <div class="simpleshop_product_quantity"> Quantity: <input type="text" size="3" value="1" name="quantity"> </div> <div class="simpleshop_product_add"> <input type="button" value="Add To Cart" name="add_product"> </div>
								</div>
								</div>
							</td>
						</tr>
					</table>
					
					<h3>How to edit a product I already created</h3>
					<ol>
						<li>This simple cart does not allow you to edit a product</li>
						<li>If you want to make a change, simply re-create your product</li>
						<li>If you want to delete a product, just remove the HTML from the page</li>
						<li>It's very quick to do</li>
					</ol>
					
					<?php
					break;
				default:
					if(isset($_POST['save']) && $_POST['save']){ 
						$product_settings = array(
							'name' => $_POST['name'],
							'shipping' => $_POST['shipping'],
							'attr' => ((is_array($_POST['attr'])) ? $_POST['attr'] : array()),
						);
						// populate product settings from the post data.
						
						$product_settings_encoded = base64_encode(serialize($product_settings));
						$hash = md5('my not so s3cret code heh'.$product_settings_encoded);
						$product_settings_encoded = $product_settings_encoded . '|' . $hash;
						
						$code_header = '<form method="post" action="'._SIMPLESHOP_BASE._SIMPLESHOP_URL.'"';
						$possible_prices = array();
						$first_price = $first_name = false;
						foreach($product_settings['attr'] as $attrid => $attr){
							$price = trim($attr['price']); // dont convert to float incase someone wants to save a note in the price??
							$name = trim($attr['name']);
							if(!$price)continue;
							if($first_price===false)$first_price = $price;
							if($first_name===false)$first_name = $name;
							$possible_prices[$attrid] = array(
								'name' => $name,
								'price' => $price,
							); 
						}
						$code = '>
						<input type="hidden" value="add" name="simpleshop_action">
						<input type="hidden" value="" name="url">
						<input type="hidden" value="'.$product_settings_encoded.'" name="simpleshop_product_settings">
						<div class="simpleshop_product">
						<div class="simpleshop_product_name">'.htmlspecialchars($product_settings['name']).'</div>
						<div class="simpleshop_product_price">
							';
						if(count($possible_prices)>1){
							$code .= '<select name="product_option">';
							foreach($possible_prices as $attrid => $attr){
								$code .= '<option value="'.$attrid.'">'.(($attr['name'])?htmlspecialchars($attr['name']) . ' - ':''). _PAYMENT_SYMBOL . htmlspecialchars($attr['price']) .'</option>';
							}
							$code .= '</select>';
						}else{
							$code .= (($first_name)?$first_name.' - ':'' ) . _PAYMENT_SYMBOL . $first_price;
						}
						$code .= '</div>
						<div class="simpleshop_product_quantity">
							Quantity: <input type="text" size="3" value="1" name="quantity">
						</div>
						<div class="simpleshop_product_add">
							<input type="submit" value="Add To Cart" name="add_product">
						</div>
						</div>
						</form>';
						$code = preg_replace('/\s+/',' ',$code);
						?>
					
						<div class="right_column">
						  <img src="<?php echo _CONFIG_FOLDER;?>title_copy.gif" width="311" height="26" alt="Copy product HTML code" />
						  <p>Copy &amp; Paste this HTML code to anywhere on your website <br />
						    to sell this product:</p>
						  <p>
						  <div style="width:386px;">
						      <textarea name="textarea" class="shop_input" cols="45" rows="5"><?php echo htmlspecialchars($code_header . $code);?></textarea>
						   <!--<div style="text-align: right; margin-top:10px;"> <img src="<?php echo _CONFIG_FOLDER;?>button_copy.gif" width="154" height="33" alt="Copy to Clipboard" /></div>-->
						   </div>
						  </p>
						  <p><img src="<?php echo _CONFIG_FOLDER;?>title_preview.gif" width="153" height="26" alt="Live Preview" /></p>
						  <p>This is how your item will appear when you paste it onto your site. You can adjust this html or style this with CSS if you like.</p>
						<div class="preview">
							<?php echo $code_header . ' target="_blank"' . $code;?>
						</div>
						<p><a href="?simpleshop_admin"><img src="<?php echo _CONFIG_FOLDER;?>button_done.gif" width="217" height="33" alt="Done, Next Product" border="0" /></a></p>
						</div><!--end right-->
							
					
					<?php } ?>
					
					<div class="left_column">
						<form action="?simpleshop_admin" method="post">
					<input type="hidden" name="save" value="true">
					  <img src="<?php echo _CONFIG_FOLDER;?>title_create_new.gif" width="266" height="26" alt="Create a new product" />
					  <p>Enter the details of your product below and click the <br />
					    generate HTML button.</p>
					  <table width="100%" border="0" cellspacing="0" cellpadding="0">
					    <tr>
					      <td width="26%"><strong>Product Name:</strong></td>
					      <td width="74%" height="35">
					        <input type="text" name="name" class="shop_input" value="<?php echo (isset($_REQUEST['name'])) ? $_REQUEST['name'] : 'My Product';?>" size="30">
					      </td>
					    </tr>
					    <tr>
					      <td><strong>Shipping Price:</strong></td>
					      <td height="35">
						      	<input type="text" name="shipping" class="shop_input" value="<?php echo (isset($_REQUEST['shipping'])) ? $_REQUEST['shipping'] : '0';?>" size="6">
					      </td>
					    </tr>
					  </table>
					  <p><strong>Product Options:</strong><br />
					    These options will be shown to the buyer in a drop-down list. If only one<br />
					    option is available just enter a price below.</p>
					  <table width="100%" border="0" cellspacing="0" cellpadding="0" id="product_options">
					    <tr>
					      <td height="35"><strong>Option Name:</strong> (Eg. Size 12)</td>
					      <td><strong>Price:</strong></td>
					      <td>&nbsp;</td>
					    </tr>
					    <?php
					    $limit = (isset($_REQUEST['attr']) ? count($_REQUEST['attr']) : 1);
					    for($x=0;$x<$limit;$x++){
					    	?>
					    <tbody class="dynamic_block">
						    <tr>
						      <td height="35">
						      	<input type="text" class="shop_input" name="attr[<?php echo $x;?>][name]" rel="name" value="<?php echo (isset($_REQUEST['attr']) && isset($_REQUEST['attr'][$x])) ? $_REQUEST['attr'][$x]['name'] : '';?>" size="25">
						      </td>
						      <td>
						      	<input type="text" class="shop_input" name="attr[<?php echo $x;?>][price]" rel="price" value="<?php echo (isset($_REQUEST['attr']) && isset($_REQUEST['attr'][$x])) ? $_REQUEST['attr'][$x]['price'] : '';?>" size="6">
						      </td>
						      <td>
							      <a href="#" onclick="selrem(this,'product_options'); reset_names(); return false;" class="remove_addit"><img src="<?php echo _CONFIG_FOLDER;?>button_remove.gif" width="18" height="18" alt="Remove Option" border="0" /></a>
							      <a href="#" onclick="seladd(this,'product_options'); reset_names(); return false;" class="add_addit"><img src="<?php echo _CONFIG_FOLDER;?>button_add.gif" width="18" height="18" alt="Add more options" border="0" /></a>
						      </td>
						    </tr>
					    </tbody>
					    <?php } ?>
					  </table>
					  <script language="javascript">
						set_add_del('product_options');
						</script>
			
					  <br />
					  <p style="text-align: center">
					    <input type="image" src="<?php echo _CONFIG_FOLDER;?>button_generate.gif" alt="Generate HTML Code" width="217" height="33" border="0" />
					  </p>
					  </form>
					</div><!--end left-->
				
					<?php
					// end default.
					break;
			}
	}
	_simpleshop_footer(true);
	
}else if(isset($_REQUEST['simpleshop_checkout'])){
	// so back button works.
	header("Cache-Control: private, max-age=10800, pre-check=10800");
	header("Pragma: private");

	header("Expires: " . date(DATE_RFC822,strtotime("+1 day")));
	
	// quick htmlspecislchars hack
	foreach($member as $key=>$val){
		$member[$key] = htmlspecialchars($val);
	}
	
	_simpleshop_header();
	?>
	<?php if(!count($cart_products)){ ?>
	
	<fieldset id="cart_products" class="simplecart_fieldset">
		<legend>Your Shopping Cart</legend>
		<p>
			Sorry, there are no items in your Shopping cart. Please add some items to your Shopping cart in order to check out.
		</p>
	</fieldset>
	<fieldset id="cart_confirm" class="simplecart_fieldset">
		<input type="button" name="continue" value="&laquo; Continue Shopping" onClick="window.location.href='<?php echo _PAYMENT_CONTINUE_URL;?>';">
	</fieldset>
		
		
	<?php }else{ ?>
	
	<script type="text/javascript">
	// quick hack to stop enter key processing
	function stopRKey(evt) {
	  var evt = (evt) ? evt : ((event) ? event : null);
	  var node = (evt.target) ? evt.target : ((evt.srcElement) ? evt.srcElement : null);
	  if ((evt.keyCode == 13) && (node.type=="text"))  {return false;}
	}
	document.onkeypress = stopRKey;
	</script> 

	<form action="?simpleshop_pay" method="post" class="cart_form" id="shop_checkout" style="margin:0;padding:0;">
	<fieldset id="cart_products" class="simplecart_fieldset">
		<legend>Your Shopping Cart</legend>
			
		<table width="100%" class="shopping_cart_checkout">
			<thead>
				<tr>
					<th align="left">Products</th>
					<th width="50">Qty.</th>
					<th align="right" width="100">Total</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($cart_products as $product_id => $product){ 
				$quantity = max(1,(int)$product['quantity']);
				$product_total = $product['price'] * $quantity;
				?>
				<tr>
					<td>
						<?=$product['name'];?>
					</td>
					<td align="center"><?=$quantity;?></td>
					<td align="right"><?php echo _PAYMENT_SYMBOL;?><?=number_format($product_total,2,".",",");?></td>
				</tr>
				<?php 
			} ?>
			<?php if($cart_shipping){ 
				?>
				<tr>
					<td colspan="2">
						<?php echo _SIMPLESHOP_SHIPPING_NAME;?>
					</td>
					<td align="right">
						<?php echo _PAYMENT_SYMBOL;?><?php
						echo number_format($cart_shipping,2,".",",");
						?>
					</td>
				</tr>
			<?php
			}
			?>
			<tr>
				<td></td>
				<td align="right"><b>Total:</b></td>
				<td align="right"><strong><?php echo _PAYMENT_SYMBOL;?><?=number_format($cart_total,2,".",",");?></strong></td>
			</tr>
			</tbody>
			<tfoot>
				<tr>
					<td align="left">
						<input type="button" name="continue" value="&laquo; Adjust Shopping Cart" onClick="window.location.href='?simpleshop_cart';">
					</td>
					<td align="right">
						
					</td>
					<td align="center">
						
					</td>
				</tr>
			</tfoot>
		</table>
	</fieldset>
	
	<fieldset id="cart_customer_information" class="simplecart_fieldset">
			<legend>Customer Information</legend>
			
			<table>
			<tbody>
			<tr>
				<td>
					<label for="member_email">E-mail address: <span title="This field is required." class="form-required">*</span></label>
				</td>
				<td>
					<input type="text" name="email" id="email" class="shop_input" value="<?php echo isset($member['email']) ? $member['email'] : '';?>">
				</td>
			</tr>
			<tr>
				<td>
					<label for="first_name">First name: <span title="This field is required." class="form-required">*</span></label> 
				</td>
				<td> 
					<input type="text" class="shop_input required" value="<?php echo isset($member['first_name']) ? $member['first_name'] : '';?>" size="32" name="first_name" maxlength="32"/>
				</td>
			</tr>
			<tr>
				<td>
					<label for="last_name">Last name:</label>
				</td>
				<td>
					<input type="text" class="shop_input" value="<?php echo isset($member['last_name']) ? $member['last_name'] : '';?>" size="32" name="last_name" maxlength="32"/>
				</td>
			</tr>
			</tbody>
			</table>
			
		</fieldset>
		
		<?php if($cart_shipping){ ?>
		<fieldset id="cart_delivery_information" class="simplecart_fieldset">
			<legend>Delivery Information</legend>
			
			<p>Enter your delivery address and information here.</p>
			
			<table>
			<tbody>
			<tr>
				<td>
					Company:
				</td>
				<td>
					<input type="text" value="<?php echo isset($member['member_company']) ? $member['member_company'] : '';?>" size="32" class="shop_input" name="member_company" maxlength="64"/>
			
			</td></tr><tr><td>Street address: <span title="This field is required." class="form-required">*</span></td><td>
			<input type="text" class="shop_input required" value="<?php echo isset($member['member_address1']) ? $member['member_address1'] : '';?>" size="32" name="member_address1" maxlength="64"/>
			
			</td></tr><tr><td> </td><td>
			<input type="text" value="<?php echo isset($member['member_address2']) ? $member['member_address2'] : '';?>" size="32" class="shop_input" name="member_address2" maxlength="64"/>
			
			</td></tr><tr><td>City: <span title="This field is required." class="form-required">*</span></td><td>
			<input type="text" class="shop_input required" value="<?php echo isset($member['member_city']) ? $member['member_city'] : '';?>" size="32" name="member_city" maxlength="32"/>
			
			</td></tr><tr><td>Country: <span title="This field is required." class="form-required">*</span></td><td>
			<select class="shop_input shop_select required" name="member_country">
			<option value="">Please select</option>
			<?php 
			$shipping_countries = explode("\n",_SIMPLESHOP_COUNTRIES);
			foreach($shipping_countries as $country){
				$country = trim($country);
				if(!$country)continue;
				?>
				<option value="<?php echo $country;?>"<?php echo (isset($member['member_country']) && $member['member_country']==$country) ? ' selected' : '';?>><?php echo $country;?></option>
				<?php
			}
			?>
			</select>
			
			</td></tr><tr><td>State: <span title="This field is required." class="form-required">*</span></td><td>
			<input type="text" class="shop_input required" value="<?php echo isset($member['member_state']) ? $member['member_state'] : '';?>" size="32" name="member_state" maxlength="32"/>
			
			</td></tr><tr><td>Post/Zip code: <span title="This field is required." class="form-required">*</span></td><td>
			<input type="text" class="shop_input required" value="<?php echo isset($member['member_post_code']) ? $member['member_post_code'] : '';?>" size="10" name="member_post_code" maxlength="10"/>
			
			</td></tr><tr><td>Phone number: </td><td>
			<input type="text" class="shop_input" value="<?php echo isset($member['member_phone']) ? $member['member_phone'] : '';?>" size="16" name="member_phone" maxlength="32"/>
			
			</td></tr></tbody></table>
			
		</fieldset>
		<?php } ?>
		
		<fieldset id="cart_order_notes" class="simplecart_fieldset">
			<legend>Order Comments</legend>
			
			<p>This will be passed along with your order. Include any special ordering or delivery information</p>
			<label for="order_comments">Order comments: </label>
			<textarea name="comments" class="shop_input" id="order_comments" rows="5" cols="60"><?php echo isset($member['comments']) ? $member['comments'] : '';?></textarea>
		</fieldset>
		
		<fieldset id="cart_order_notes" class="simplecart_fieldset">
			<legend>Payment Options</legend>
			
			<table cellpadding="4">
				<?php 
				$has_payment_method = false;
				if(defined('_PAYMENT_PAYPAL_EMAIL') && _PAYMENT_PAYPAL_EMAIL){ 
					?>
					<tr>
						<td>
							<input type="radio" name="payment_option" id="paypal_checkout" value="paypal"<?php echo (!$has_payment_method)?' checked':'';?>>
						</td>
						<td>
							<h3><label for="paypal_checkout">PayPal</label></h3>
							<p>Pay securely using your PayPal account or Credit Card.</p>
						</td>
					</tr>
					<?php 
					$has_payment_method = true;
				}
				if(defined('_PAYMENT_GOOGLE_MERCHID') && _PAYMENT_GOOGLE_MERCHID){ 
					?>
					<tr>
						<td>
							<input type="radio" name="payment_option" id="google_checkout" value="google"<?php echo (!$has_payment_method)?' checked':'';?>>
						</td>
						<td>
							<h3><label for="google_checkout">Google Checkout</label></h3>
							<p>Pay securely online using Google Checkout</p>
						</td>
					</tr>
					<?php 
					$has_payment_method = true;
				}
				if(defined('_PAYMENT_BANK_TRANSFER') && trim(_PAYMENT_BANK_TRANSFER)){ 
					?>
					<tr>
						<td>
							<input type="radio" name="payment_option" id="bank_transfer" value="bank"<?php echo (!$has_payment_method)?' checked':'';?>>
						</td>
						<td>
							<h3><label for="bank_transfer">Bank Transfer</label></h3>
							<p>Pay via direct Bank Transfer</p>
						</td>
					</tr>
					<?php 
					$has_payment_method = true;
				} 
				
				// todo, add others like plain credit card.
				?>
			</table>
			
		</fieldset>
		<fieldset id="cart_pay" class="simplecart_fieldset">
			<input type="submit" name="pay" value="Place Order &raquo;">
		</fieldset>
		
	</form>
	<?php } ?> 
	<?php
	_simpleshop_footer();
}else if(isset($_REQUEST['simpleshop_pay'])){
	if(isset($_POST) && is_array($_POST) && count($_POST) > 3){
		$member = $_SESSION['_cart_member_data'] = $_POST; // hack to remember members data.
	}
	// check required fields.
	$required_fields = array(
		'email',
		'first_name',
		'payment_option',
	);
	if($cart_shipping){
		$required_fields[] = 'member_address1';
		$required_fields[] = 'member_city';
		$required_fields[] = 'member_country';
		$required_fields[] = 'member_state';
		$required_fields[] = 'member_post_code';
	}
	$missing = array();
	foreach($required_fields as $required_field){
		if(!isset($_REQUEST[$required_field]) || !trim($_REQUEST[$required_field])){
			$missing[] = $required_field;
		}
	}
	if($missing){
		_simpleshop_header();
		?>
		<fieldset id="cart_required" class="simplecart_fieldset">
			<legend>Required Fields</legend>
			<p>I'm sorry, some required fields were missing. Please go <a href="javascript:history.go(-1);">back</a> and complete all required (*) fields.</p>
		</fieldset>
		<?php
		_simpleshop_footer();
	}else{
	
		// finalize the order, send the emails, and redirect to payment gateway.
		$email_text = 'An order has been placed through your website: '._SIMPLESHOP_BASE._SIMPLESHOP_URL."\n\n";
		$email_text .= "ORDER DETAILS:\n";
		$email_text .= "--------------------------------\n";
		foreach($cart_products as $cart_product){
			$email_text .= " - ".$cart_product['quantity'] . ' x ' . $cart_product['name'] . ' @'.$cart_product['price']._PAYMENT_CURRENCY."/each \n";
		}
		if($cart_shipping){
			$email_text .= " - "._SIMPLESHOP_SHIPPING_NAME." @".$cart_shipping._PAYMENT_CURRENCY." \n";
		}
		$email_text .= " Total Price: ".$cart_total._PAYMENT_CURRENCY."\n";
		$email_text .= "--------------------------------\n";
		$email_text .= "\n\n";
		$email_text .= "PAYMENT METHOD: " . $_REQUEST['payment_option'] . "\n\n";
		$email_text .= "If the payment is through Google or PayPal, you will receive an email once the payment is successful.\n";
		$email_text .= "If the payment is through Bank Transfer, the user has been given your bank details to make payment.\n";
		$email_text .= "\n";
		$email_text .= "CUSTOMER DETAILS:\n";
		$email_text .= "--------------------------------\n";
		unset($member['payment_option']);
		unset($member['pay']);
		foreach($member as $key=>$val){
			$email_text .= " $key: \t\t $val \n";
		}
		$email_text .= "--------------------------------\n";
		$email_text .= "\n\n";
		if(mail(_SHOP_EMAIL,"Website Order Placed",$email_text,'From: '._SHOP_EMAIL)){
			//echo "Sent to "._SHOP_EMAIL.": <br><pre>$email_text</pre>";
		}
		
		
		if(isset($_SESSION['_simpleshop_cart_products'])){
			// remove teh cart from session after ordering:
			unset($_SESSION['_simpleshop_cart_products']);
		}
		if(isset($_SESSION['_cart_member_data']['comments'])){
			// remember all members details except their comments.
			unset($_SESSION['_cart_member_data']['comments']);
		}
		$customer_email = nl2br(_SHOP_CUSTOMER_EMAIL);
		$customer_email = str_replace('SHOP_NAME',_SHOP_NAME,$customer_email);
		$customer_email = str_replace('FIRST_NAME',$member['first_name'],$customer_email);
		$cart_contents_email = '';
		foreach($cart_products as $cart_product){
			$cart_contents_email .= "  ".$cart_product['quantity'] . ' x ' . $cart_product['name'] . ' @'.$cart_product['price']._PAYMENT_CURRENCY."/each <br> \n";
		}
		if($cart_contents_email){
			$cart_contents_email .= " - "._SIMPLESHOP_SHIPPING_NAME." @".$cart_shipping._PAYMENT_CURRENCY." <br> \n";
		}
		$cart_contents_email .= " Total Price: <strong>".number_format($cart_total,2,'.',',').' '._PAYMENT_CURRENCY."</strong>\n";
		$customer_email = str_replace('CART_CONTENTS',$cart_contents_email,$customer_email);
		// just redirect them to the chosen payment method.
		switch($_REQUEST['payment_option']){
			case 'google':
				$url = "https://checkout.google.com/api/checkout/v2/checkoutForm/Merchant/"._PAYMENT_GOOGLE_MERCHID.'';
		
				$fields = array(
					"edit_url"=>_SIMPLESHOP_BASE._SIMPLESHOP_URL,
					"continue_url"=>((!preg_match('#^https?://#i',_PAYMENT_CONTINUE_URL)) ? _SIMPLESHOP_BASE : '' ) ._PAYMENT_CONTINUE_URL,
				);
				// now add the products.
				$product_number = 1;
				foreach($cart_products as $cart_product){
					$fields['item_name_'.$product_number] = $cart_product['name'];
					$fields['item_description_'.$product_number] = $cart_product['name'];
					$fields['item_quantity_'.$product_number] = $cart_product['quantity'];
					$fields['item_price_'.$product_number] = $cart_product['price'];
					$fields['item_currency_'.$product_number] = _PAYMENT_CURRENCY;
					$product_number ++ ;
				}
				if($cart_shipping){
					$fields['ship_method_name_1'] = _SIMPLESHOP_SHIPPING_NAME;
					$fields['ship_method_price_1'] = $cart_shipping;
					$fields['ship_method_currency_1'] = _PAYMENT_CURRENCY;
				}
				?>
				<form method="POST" id="goform" action="<?php echo $url;?>" accept-charset="utf-8">
				<?php foreach($fields as $key=>$val){
					echo '<input type="hidden" name="'.$key.'" value="'.htmlspecialchars($val).'">';
				}
				?>
				<input type="submit" value="go" name="go" id="gobutton">
				</form>
				<script language="javascript">
				document.getElementById('gobutton').style.display='none';
				document.getElementById('goform').submit();
				</script>
				<?php
				break;
			case 'paypal':
				//$url = "https://www.sandbox.paypal.com/cgi-bin/webscr?";
				// TODO: set flag for sandbox processing
				$url = "https://www.paypal.com/cgi-bin/webscr?";
		
				$fields = array(
					"cmd"=>'_cart',
					"upload"=>'1',
					"rm"=>'1',
					"business"=>_PAYMENT_PAYPAL_EMAIL,
					"currency_code"=>_PAYMENT_CURRENCY,
					"handling_cart"=>'0',
					//"notify_url"=>"", // set this to cart for later, when we're processing ipn's
					"cancel_return"=>_SIMPLESHOP_BASE._SIMPLESHOP_URL,
					"return"=>((!preg_match('#^https?://#i',_PAYMENT_CONTINUE_URL)) ? _SIMPLESHOP_BASE : '' ) ._PAYMENT_CONTINUE_URL,
					"image_url"=>'',
				);
				//$fields['cpp_header_image'] = ""; // TODO - option for a banner image on checkout page
				// now add the products.
				$product_number = 1;
				foreach($cart_products as $cart_product){
					$fields['item_name_'.$product_number] = $cart_product['name'];
					$fields['quantity_'.$product_number] = $cart_product['quantity'];
					$fields['amount_'.$product_number] = $cart_product['price'];
					//$fields['tax_'.$product_number] = $cart_product['tax'];
					$product_number ++ ;
				}
				
				// add all customer information to the paypal url:
				
				if($cart_shipping > 0){
					//$fields['shipping'] = (float)$order_details['shipping_details']['shipping_price'];
					$fields['item_name_'.$product_number] = _SIMPLESHOP_SHIPPING_NAME;
					$fields['quantity_'.$product_number] = 1;
					$fields['amount_'.$product_number] = $cart_shipping;
					$fields['tax_'.$product_number] = 0;
					$product_number ++ ;
					
					$fields['email'] = $_POST['email'];
					$fields['first_name'] = $_POST['first_name'];
					$fields['last_name'] = $_POST['last_name'];
					
					$fields['address1'] = $_POST['member_address1'];
					$fields['address2'] = $_POST['member_address2'];
					$fields['city'] = $_POST['member_city'];
					$fields['country'] = $_POST['member_country'];
					$fields['zip'] = $_POST['member_post_code'];
				}else{
					$fields['no_shipping'] = 1;
				}
				
				//print_r($fields);exit;
				foreach($fields as $key=>$val){
					$url .= $key . "=" . urlencode($val) . "&";
				}
				header("Location: $url");
				break;
			case 'bank':
				_simpleshop_header();
				ob_start();
				?>
				<p>Please make final payment of <strong><?php echo _PAYMENT_SYMBOL;?><?php echo number_format($cart_total,2,'.',',');?> <?php echo _PAYMENT_CURRENCY;?></strong> to the following bank details.</p>
				<div style="padding:5px; background-color:#EFEFEF;"><?php echo nl2br(_PAYMENT_BANK_TRANSFER);?></div>
				<?php
				$customer_email = str_replace('BANK_DETAILS',ob_get_clean(),$customer_email);
				?>
				<fieldset id="cart_required" class="simplecart_fieldset">
					<legend>Bank Payment</legend>
					<p>Thank you for placing your order. Please make final payment of <strong><?php echo _PAYMENT_SYMBOL;?><?php echo number_format($cart_total,2,'.',',');?> <?php echo _PAYMENT_CURRENCY;?></strong> to the following bank details.</p>
					<div style="padding:5px; background-color:#EFEFEF;"><?php echo nl2br(_PAYMENT_BANK_TRANSFER);?></div>
					<p>We have also sent you an email to <strong><?php echo htmlspecialchars($_POST['email']);?></strong> with a copy of your order and these payment details.</p>
					<p>Please <a href="<?php echo _PAYMENT_CONTINUE_URL;?>">click here</a> to continue.</p>
				</fieldset>
				<?php
				_simpleshop_footer();
				break;
		}
		$customer_email = str_replace('BANK_DETAILS','',$customer_email);
		// send the email.
		$headers = 'From: ' . _SHOP_NAME . ' <'._SHOP_EMAIL . ">\r\n";
		$headers .= 'MIME-Version: 1.0' . "\r\n";
		$headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";
		// bah - configmr this is a valid email address
		mail($member['email'],_SHOP_SUBJECT,$customer_email,$headers);
	}
}else{ 
	
	// show the users cart by default:
	
	_simpleshop_header();
	if(!count($cart_products)){ ?>
	
	<fieldset id="cart_products" class="simplecart_fieldset">
		<legend>Your Shopping Cart</legend>
		<p>
			Sorry, there are no items in your Shopping cart. Please add some items to your Shopping cart in order to check out.
		</p>
	</fieldset>
	<fieldset id="cart_confirm" class="simplecart_fieldset">
		<input type="button" name="continue" value="&laquo; Continue Shopping" onClick="window.location.href='<?php echo _PAYMENT_CONTINUE_URL;?>';">
	</fieldset>
		
		
	<?php }else{ ?>
	<form action="?simpleshop_cart" method="post" class="cart_form" id="shop_checkout" style="margin:0;padding:0;">
	<input type="hidden" name="simpleshop_action" value="modify_quantities">
	<fieldset id="cart_products" class="simplecart_fieldset">
		<legend>Your Shopping Cart</legend>
		
		<table width="100%" class="shopping_cart_checkout">
			<thead>
				<tr>
					<th width="50">Remove</th>
					<th align="left">Products</th>
					<th width="50">Qty.</th>
					<th align="right" width="100">Total</th>
				</tr>
			</thead>
			<tbody>
			<?php
			foreach($cart_products as $product_id => $product){ 
				$quantity = max(1,(int)$product['quantity']);
				$product_total = $product['price'] * $quantity;
				?>
				<tr>
					<td><input type="button" name="del" value="Remove" onClick="document.getElementById('mod_prod_<?=$product_id;?>').value='0'; this.form.submit();"></td>
					<td>
						<?=$product['name'];?>
					</td>
					<td><input type="text" name="product_quantities[<?=$product_id;?>]" id="mod_prod_<?=$product_id;?>" value="<?=$quantity;?>" size="4"></td>
					<td align="right"><?php echo _PAYMENT_SYMBOL;?><?=number_format($product_total,2,".",",");?></td>
				</tr>
				<?php 
			} ?>
			<?php if($cart_shipping){ 
				?>
				<tr>
					<td></td>
					<td colspan="2">
						<?php echo _SIMPLESHOP_SHIPPING_NAME;?>
					</td>
					<td align="right">
						<?php echo _PAYMENT_SYMBOL;?><?php
						echo number_format($cart_shipping,2,".",",");
						?>
					</td>
				</tr>
			<?php
			}
			?>
			<tr>
				<td></td>
				<td></td>
				<td align="right"><b>Total:</b></td>
				<td align="right"><strong><?php echo _PAYMENT_SYMBOL;?><?=number_format($cart_total,2,".",",");?></strong></td>
			</tr>
			</tbody>
			<tfoot>
				<tr>
					<td colspan="2" align="left">
						<input type="button" name="continue" value="&laquo; Continue Shopping" onClick="window.location.href='<?php echo _PAYMENT_CONTINUE_URL;?>';">
					</td>
					<td align="right">
						<input type="submit" name="update_quantity" value="Update">
					</td>
					<td align="center">
						<input type="submit" name="simpleshop_checkout_go" value="Checkout &raquo;">
					</td>
				</tr>
			</tfoot>
		</table>

	</fieldset>
	</form>
	<?php
	}
	_simpleshop_footer();
}

ob_end_flush();

// incase they have php code after this, restore settings:
//ini_set("error_reporting",$simpleshop_ini_error);
//ini_set("display_errors",$simpleshop_ini_display);
