<html>
<head>
	<title>Simple PHP Shop</title>
	<style type="text/css">
	/**** YOU CAN DELETE THIS CSS - IT IS JUST AN EXAMPLE ON HOW TO STYLE THE CHECKOUT PROCESS ***/
	fieldset.simplecart_fieldset{
		border:1px solid #d5ecfa;
		margin-bottom:15px;
	}
	fieldset.simplecart_fieldset legend{
		color:#235FAC;
		font-family:Arial, Helvetica, sans-serif;;
		font-size:17px;
		font-weight:normal;
		margin-top:0;
	}
	fieldset.simplecart_fieldset table,
	fieldset.simplecart_fieldset table td,
	fieldset.simplecart_fieldset table th,
	fieldset.simplecart_fieldset{
		font-family:arial;
		font-size:13px;
	}
	</style>
</head>
<body>



<!-- paste your header html / php code here -->
<!-- anything here will be displayed above the checkout page -->



<div style="width:700px; margin:0 auto; padding:15px; border:1px dotted #EFEFEF;">
	<div style="border:1px dotted #EFEFEF; padding:40px; text-align:center; color:#CCC; margin:0 0 15px 0">
		YOUR WEBSITE HEADER/GRAPHICS APPEARS HERE
	</div>
	